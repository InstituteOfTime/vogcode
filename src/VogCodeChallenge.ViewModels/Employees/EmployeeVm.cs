﻿using System;

namespace VogCodeChallenge.ViewModels.Employees
{
	public class EmployeeVm
	{
		public Guid Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string JobTitle { get; set; }
		public string Address { get; set; }

		public Guid DepartmentId { get; set; }
	}
}
