﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VogCodeChallenge.Domain.Entities;
using VogCodeChallenge.ORM.Repositories;
using VogCodeChallenge.Services.Interfaces.Employees;
using VogCodeChallenge.ViewModels.Employees;

namespace VogCodeChallenge.Services.Employees
{
	public class EmployeeService : IEmployeeService
	{
		private readonly IGenericRepository<Employee> _employeeRepository;

		public EmployeeService(IGenericRepository<Employee> employeeRepository)
		{
			_employeeRepository = employeeRepository;
		}

		public async Task<IList<EmployeeVm>> GetAllEmployees(CancellationToken ct = default)
		{
			var employeeVmList = await _employeeRepository.ProjectToAsync<EmployeeVm>(e => true, ct: ct);
			return employeeVmList;
		}

		public async Task<IList<EmployeeVm>> GetEmployeesByDepartment(Guid departmentId, CancellationToken ct = default)
		{
			var employeeVmList = await _employeeRepository.ProjectToAsync<EmployeeVm>(e => e.DepartmentId == departmentId, ct: ct);
			return employeeVmList;
		}
	}
}
