﻿using System;
using System.Collections.Generic;
using System.Text;
using VogCodeChallenge.Domain.Infrastructure;
using VogCodeChallenge.Services.Employees;
using VogCodeChallenge.Services.Interfaces.Employees;

namespace VogCodeChallenge.Services.DI
{
	public class ServicesDIConfiguration : IDependencyInjectionConfigurator
	{
		public void RegisterService(Action<Type, Type> action)
		{
			action(typeof(IEmployeeService), typeof(EmployeeService));
		}
	}
}
