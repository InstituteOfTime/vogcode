﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using VogCodeChallenge.Domain.Entities;
using VogCodeChallenge.ViewModels.Employees;

namespace VogCodeChallenge.Services.Mapping
{
	public class OrmMappingProfile : Profile
	{
		public OrmMappingProfile()
		{
			CreateMap<Employee, EmployeeVm>()
				.ForMember(x => x.Id, x => x.MapFrom(s => s.Id))
				.ForMember(x => x.FirstName, x => x.MapFrom(s => s.FirstName))
				.ForMember(x => x.LastName, x => x.MapFrom(s => s.LastName))
				.ForMember(x => x.JobTitle, x => x.MapFrom(s => s.JobTitle))
				.ForMember(x => x.Address, x => x.MapFrom(s => s.Address))
				.ForMember(x => x.DepartmentId, x => x.MapFrom(s => s.DepartmentId));

		}
	}
}
