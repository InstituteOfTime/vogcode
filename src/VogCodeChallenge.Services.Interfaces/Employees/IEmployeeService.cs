﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VogCodeChallenge.ViewModels.Employees;

namespace VogCodeChallenge.Services.Interfaces.Employees
{
	public interface IEmployeeService
	{
		Task<IList<EmployeeVm>> GetAllEmployees(CancellationToken ct = default);
		Task<IList<EmployeeVm>> GetEmployeesByDepartment(Guid departmentId, CancellationToken ct = default);
	}
}
