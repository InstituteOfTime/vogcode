﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VogCodeChallenge.Console
{
	public static class QuestionClass
	{
		static List<string> NamesList = new List<string>()
		{
			"Jimmy",
			"jeffrey"
		};

		public static void TestQuestion()
		{
			using (var enumerator = NamesList.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					System.IO.
					Console.WriteLine(enumerator.Current);
				}
			}
		}
	}

}
