﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VogCodeChallenge.QuestionClassConsole
{
	public static class QuestionClass
	{
		static List<string> NamesList = new List<string>()
		{
			"Jimmy",
			"jeffrey"
		};

		public static void TestQuestion()
		{
			//using (var enumerator = NamesList.GetEnumerator())
			//{
			//	while (enumerator.MoveNext())
			//	{
			//		Console.WriteLine(enumerator.Current);
			//	}
			//}

			foreach (var name in NamesList)
			{
				Console.WriteLine(name);
			}
		}
	}

}
