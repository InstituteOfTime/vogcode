﻿using System.Collections;
using System.Collections.Generic;
using VogCodeChallenge.Domain.Infrastructure;

namespace VogCodeChallenge.Domain.Entities
{
	public class Department : BaseEntity
	{
		public string Name { get; set; }

		public virtual ICollection<Employee> EmployeeList { get; set; }
	}
}
