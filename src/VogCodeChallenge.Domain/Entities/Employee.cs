﻿using System;
using VogCodeChallenge.Domain.Infrastructure;

namespace VogCodeChallenge.Domain.Entities
{
	public class Employee : BaseEntity
	{
		public Guid DepartmentId { get; set; }

		public string FirstName { get; set; }
		public string LastName { get; set; }
		//TODO: denormalize, introduce new entity for job
		public string JobTitle { get; set; }
		public string Address { get; set; }
	}
}
