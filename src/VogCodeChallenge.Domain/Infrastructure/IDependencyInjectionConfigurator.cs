﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VogCodeChallenge.Domain.Infrastructure
{
	public interface IDependencyInjectionConfigurator
	{
		void RegisterService(Action<Type, Type> action);
	}
}
