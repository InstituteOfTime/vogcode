﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VogCodeChallenge.Domain.Infrastructure
{
	public interface IBaseEntity
	{
		Guid Id { get; set; }
		bool IsDeleted { get; set; }
		DateTime CreatedDate { get; set; }
		DateTime UpdatedDate { get; set; }
	}
}
