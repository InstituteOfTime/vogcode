﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VogCodeChallenge.Domain.Infrastructure
{
	public class BaseEntity : IBaseEntity
	{
		public Guid Id { get; set; }
		public bool IsDeleted { get; set; }
		public DateTime CreatedDate { get; set; }
		public DateTime UpdatedDate { get; set; }
	}
}
