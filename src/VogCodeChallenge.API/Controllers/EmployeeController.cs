﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VogCodeChallenge.Services.Interfaces.Employees;
using VogCodeChallenge.ViewModels.Employees;

namespace VogCodeChallenge.API.Controllers
{
	[Route("api/[controller]")]
	[Produces("application/json")]
	[ApiController]
	public class EmployeeController : ControllerBase
	{
		private readonly IEmployeeService _employeeService;

		public EmployeeController(IEmployeeService employeeService)
		{
			_employeeService = employeeService;
		}

		//TODO: wrap the response: use custom CollectionVm + pagination
		[HttpGet]
		public Task<IList<EmployeeVm>> GetAllEmployees(CancellationToken ct = default)
		{
			var employees = _employeeService.GetAllEmployees(ct);
			return employees;
		}

		[HttpGet("department/{departmentId}")]
		public Task<IList<EmployeeVm>> GetAllEmployees([FromRoute] Guid departmentId, CancellationToken ct = default)
		{
			var employees = _employeeService.GetEmployeesByDepartment(departmentId, ct);
			return employees;
		}
	}
}
