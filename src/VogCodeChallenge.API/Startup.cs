﻿using System;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using VogCodeChallenge.API.Extensions;
using VogCodeChallenge.Domain.Entities;
using VogCodeChallenge.ORM;
using VogCodeChallenge.ORM.DI;
using VogCodeChallenge.Services.DI;
using VogCodeChallenge.Services.Mapping;

namespace VogCodeChallenge.API
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<VogDbContext>(opt => opt.UseInMemoryDatabase("vog_dev"));

			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

			#region AutoMapper
			
			services.AddAutoMapper(typeof(OrmMappingProfile));

			#endregion


			#region Swagger

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "Vog API", Version = "v1" });
			});
			
			#endregion


			#region DI
			
			services.RegisterDependencies(new RepositoryDIConfigurator());
			services.RegisterDependencies(new ServicesDIConfiguration());
			
			#endregion
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			#region Swagger 

			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "Vog API V1");
			});

			#endregion


			app.UseHttpsRedirection();
			app.UseMvc();

			#region Seed

			using (var serviceScope = app.ApplicationServices.CreateScope())
			{
				var context = serviceScope.ServiceProvider.GetService<VogDbContext>();
				AddTestData(context);
			}

			#endregion
		}


		//TODO: remove after switch to the real DB
		private static void AddTestData(VogDbContext context)
		{
			var departments = new[] { "Central office", "West department" };
			var employees = new[]
			{
				new { Dep = 0, First = "John", Last = "Smith", Title = "CEO", Address = "27 Tuet Mong South Avenue Kwa Chat Nuk Hong Kong" },
				new { Dep = 0, First = "David", Last = "Fox", Title = "CFO", Address = "310 Matteo Station Elyssahaven, MT 93768-1998" },
				new { Dep = 1, First = "Robert", Last = "Anderson", Title = "Senior Business Analyst", Address = "641 Rohan Trail Apt. 032 New Toby, IA 03923-4173" },
				new { Dep = 1, First = "Jason", Last = "McKinsey", Title = "Senior Accountant", Address = "1903 Welch Junctions Apt. 553 East Kristopher, MA 48888-3720" },
				new { Dep = 1, First = "Paul", Last = "Foster", Title = "Senior Developer", Address = "712 Blair Spurs Apt. 886 New Helenestad, PA 44924-8537" },
			};

			var depIdList = departments.Select(name =>
			{
				var department = new Department
				{
					Id = Guid.NewGuid(),
					Name = name,
					CreatedDate = DateTime.UtcNow,
					UpdatedDate = DateTime.UtcNow,
				};

				context.Add(department);
				return department.Id;
			})
			.ToList();

			foreach (var e in employees)
			{
				var employee = new Employee
				{
					Id = Guid.NewGuid(),
					CreatedDate = DateTime.UtcNow,
					UpdatedDate = DateTime.UtcNow,
					FirstName = e.First,
					LastName = e.Last,
					JobTitle = e.Title,
					Address = e.Address,
					DepartmentId = depIdList[e.Dep]
				};

				context.Add(employee);
			}

			context.SaveChanges();
		}
	}
}
