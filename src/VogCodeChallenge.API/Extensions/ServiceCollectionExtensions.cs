﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using VogCodeChallenge.Domain.Infrastructure;

namespace VogCodeChallenge.API.Extensions
{
	public static class ServiceCollectionExtensions
	{
		public static void RegisterDependencies<T>(this IServiceCollection services, T configurator) where T : IDependencyInjectionConfigurator
		{
			configurator.RegisterService(SimpleRegister);

			void SimpleRegister(Type inter, Type impl)
			{
				services.AddScoped(inter, impl);
			}
		}
	}
}
