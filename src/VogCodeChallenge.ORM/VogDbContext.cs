﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using VogCodeChallenge.Domain.Entities;

namespace VogCodeChallenge.ORM
{
	public class VogDbContext : DbContext
	{
		public VogDbContext(DbContextOptions<VogDbContext> options) : base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);

			builder.Entity<Department>();
			builder.Entity<Employee>();
		}
	}
}
