﻿using System;
using System.Collections.Generic;
using System.Text;
using VogCodeChallenge.Domain.Infrastructure;
using VogCodeChallenge.ORM.Repositories;

namespace VogCodeChallenge.ORM.DI
{
	public class RepositoryDIConfigurator : IDependencyInjectionConfigurator
	{
		public void RegisterService(Action<Type, Type> action)
		{
			action(typeof(IGenericRepository<>), typeof(GenericRepository<>));
		}
	}
}
