﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VogCodeChallenge.Domain.Infrastructure;

namespace VogCodeChallenge.ORM.Repositories
{
	public interface IGenericRepository<T> where T : class, IBaseEntity
	{
		Task<List<TResult>> ProjectToAsync<TResult>(Expression<Func<T, bool>> predicate, bool findDeleted = false,
			CancellationToken ct = default);

		Task Insert(T entity, CancellationToken ct = default);

		Task Insert(IEnumerable<T> entities, CancellationToken ct = default);
	}
}
