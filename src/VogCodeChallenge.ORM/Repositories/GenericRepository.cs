﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using VogCodeChallenge.Domain.Infrastructure;

namespace VogCodeChallenge.ORM.Repositories
{
	public class GenericRepository<T> : IGenericRepository<T> where T : class, IBaseEntity
	{
		protected readonly VogDbContext _vogDbContext;
		protected readonly IMapper _mapper;

		public GenericRepository(VogDbContext vogDbContext, IMapper mapper)
		{
			_vogDbContext = vogDbContext;
			_mapper = mapper;
		}

		protected DbSet<T> Entities => _vogDbContext.Set<T>();
		protected IQueryable<T> NotDeletedEntities => Entities.Where(e => !e.IsDeleted);

		public async Task<List<TResult>> ProjectToAsync<TResult>(Expression<Func<T, bool>> predicate, bool findDeleted = false, 
			CancellationToken ct = default)
		{
			var query = findDeleted ? Entities.AsQueryable() : NotDeletedEntities;
			return await query.Where(predicate).ProjectTo<TResult>(_mapper.ConfigurationProvider).ToListAsync(ct);
		}

		public Task Insert(T entity, CancellationToken ct = default)
		{
			return Entities.AddAsync(entity, ct);
		}

		public Task Insert(IEnumerable<T> entities, CancellationToken ct = default)
		{
			return Entities.AddRangeAsync(entities, ct);
		}

		// TODO 

	}
}
